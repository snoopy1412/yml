#!/bin/sh
mkdir -p ~/.ssh
chmod 700 ~/.ssh
eval $(ssh-agent -s)
echo "$TEST_KNOWN_HOST" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
echo "$TEST_CONFIG" > ~/.ssh/config
chmod 600 ~/.ssh/config
echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa